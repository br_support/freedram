(********************************************************************
 * COPYRIGHT -- PAS s.r.o.
 ********************************************************************
 * File: Global.typ
 * Author: Jaroslav Braum
 * Created: May 05, 2011
 ********************************************************************
 * Global data types of project KotelCH
 ********************************************************************)

TYPE
	tOPC : 	STRUCT 
		bits : ARRAY[0..31]OF BOOL;
		U8 : ARRAY[0..10]OF USINT;
		U16 : ARRAY[0..10]OF UINT;
		U32 : ARRAY[0..10]OF UDINT;
		I16 : ARRAY[0..10]OF INT;
	END_STRUCT;
	tYear : 	STRUCT 
		leden : UDINT;
		unor : UDINT;
		brezen : UDINT;
		duben : UDINT;
		kveten : UDINT;
		cerven : UDINT;
		cervenec : UDINT;
		srpen : UDINT;
		zari : UDINT;
		rijen : UDINT;
		listopad : UDINT;
		prosinec : UDINT;
	END_STRUCT;
	tTime : 	STRUCT 
		bNewSecond : BOOL;
		usOldSec : USINT;
		usOldDay : USINT;
		usOldMonth : USINT;
		sRTC : RTCtime_typ;
	END_STRUCT;
	tPrimaryAirCfg : 	STRUCT 
		rSPwhileDelayed : REAL; (*set value for damper while delayed after the bale is stoked*)
		rSPpercentWhileO2low : REAL; (*while low O2 lower primary air to this percent of set value*)
		rDelay : REAL; (*wait after the bale is stoked*)
		rO2low : REAL; (*limit for O2 level*)
		arSP : ARRAY[0..6]OF REAL;
		rMaxRPM : REAL; (*nominalni otacky pohonu (ze stitku)*)
		rO2mid : REAL;
		rKp : REAL; (*pid proporcional gain*)
		rTn : REAL; (*pid integral action time [s]*)
	END_STRUCT;
	tModuleDiag : 	STRUCT 
		bOK : BOOL; (*module ok*)
		usStatus : USINT; (*module status *)
	END_STRUCT;
	tAOphy : 	STRUCT 
		rZ16 : REAL; (*tricestny ventil Z16, kotlovy okruh*)
	END_STRUCT;
	tAIphy : 	STRUCT 
		rTI01 : REAL; (*teplota v topenisti (0 - 1100 C)*)
		rTI02 : REAL; (*teplota spalin*)
		rTI03 : REAL; (*teplota topne vody*)
		rTI04 : REAL; (*teplota vratne vody*)
		rTI08 : REAL; (*teplota vzduchu v kotelne*)
		rTI05 : REAL; (*teplota topne vody za smesovacim ventilem*)
		rPI01 : REAL; (*podtlak spalin (-250 az 0Pa)*)
		rQI01 : REAL; (*mereni kysliku ve spalinach*)
		rQI02 : REAL; (*mereni vykonu kotle*)
		rZ16 : REAL; (*poloha tricestneho ventilu Z16*)
		rTC12_05 : REAL; (*teplota z akumulace do anuloidu*)
		rTC12_06 : REAL; (*teplota zp�te�ka do akumulace*)
		rTC12_01 : REAL; (*teplota v akumulaci 1 horn�*)
		rTC12_02 : REAL; (*teplota v akumulaci 1 spodn�*)
		rTC12_03 : REAL; (*teplota v akumulaci 2 horn�*)
		rTC12_04 : REAL; (*teplota v akumulaci 2 spodn�*)
		rHA04 : REAL; (*detektor CO*)
	END_STRUCT;
	tAOs : 	STRUCT 
		iZ16 : INT; (*tricestny ventil Z16, kotlovy okruh*)
	END_STRUCT;
	tAIs : 	STRUCT 
		iTI01 : INT; (*teplota v topenisti (0 - 1100 C)*)
		iTI02 : INT; (*teplota spalin*)
		iTI03 : INT; (*teplota topne vody*)
		iTI04 : INT; (*teplota vratne vody*)
		iTI08 : INT; (*teplota vzduchu v kotelne*)
		iTI05 : INT; (*teplota topne vody za smesovacim ventilem*)
		iPI01 : INT; (*podtlak spalin (-250 az 0Pa)*)
		iQI01 : INT; (*mereni kysliku ve spalinach*)
		iQI02 : INT; (*mereni vykonu kotle*)
		iZ16 : INT; (*poloha tricestneho venitlu Z16*)
		iTC12_05 : INT; (*teplota z akumulace do anuloidu*)
		iTC12_06 : INT; (*teplota zp�te�ka do akumulace*)
		iTC12_01 : INT; (*teplota v akumulaci 1 horn�*)
		iTC12_02 : INT; (*teplota v akumulaci 1 spodn�*)
		iTC12_03 : INT; (*teplota v akumulaci 2 horn�*)
		iTC12_04 : INT; (*teplota v akumulaci 2 spodn�*)
		iHA04 : INT; (*detektor CO*)
	END_STRUCT;
	tDIs : 	STRUCT 
		bZ04auto : BOOL; (*retezovy dopravnik*)
		bZ04ready : BOOL; (*retezovy dopravnik*)
		bZ04on1 : BOOL; (*retezovy dopravnik*)
		bZ04on2 : BOOL; (*retezovy dopravnik*)
		bZ08auto : BOOL; (*snekovy dopravnik odpopelneni vnitrni, automaticky rezim*)
		bZ08ready : BOOL; (*snekovy dopravnik odpopelneni vnitrni, adproudovka ok*)
		bZ08on1 : BOOL; (*snekovy dopravnik odpopelneni vnitrni, chod*)
		bZ09auto : BOOL; (*snekovy dopravnik odpopelneni vnejsi, automaticky rezim*)
		bZ09ready : BOOL; (*snekovy dopravnik odpopelneni vnejsi, nadproudovka ok*)
		bZ09on1 : BOOL; (*snekovy dopravnik odpopelneni vnejsi, chod*)
		bZ03auto : BOOL; (*hydraulicky agregat, automaticky rezim*)
		bZ03ready : BOOL; (*hydraulicky agregat, nadproudovka ok*)
		bZ03on : BOOL; (*hydraulicky agregat, chod*)
		bZ05ready : BOOL; (*chladici cerpadlo kotle, nadproudovka ok*)
		bZ05on : BOOL; (*chladici cerpadlo kotle, chod*)
		bZ10auto : BOOL; (*snekovy dopravnik odpopelneni vnejsi, automaticky rezim*)
		bZ10ready : BOOL; (*snekovy dopravnik odpopelneni vnejsi, nadproudovka ok*)
		bZ10on1 : BOOL; (*snekovy dopravnik odpopelneni vnejsi, chod*)
		bZ21on : BOOL; (*vetrani kotelny, chod*)
		bM04ptc : BOOL; (*teplota motoru M04 prekrocena (PTC)*)
		bM08ptc : BOOL; (*teplota motoru M08 prekrocena (PTC)*)
		bM09ptc : BOOL; (*teplota motoru M09 prekrocena (PTC)*)
		bM10ptc : BOOL; (*teplota motoru M10 prekrocena (PTC)*)
		bK1_od : BOOL; (*Konc. poloha klapky K1 otevreno*)
		bK1_cd : BOOL; (*Konc. poloha klapky K1 zavreno*)
		bK2_od : BOOL; (*Konc. poloha klapky K2 otevreno*)
		bK2_cd : BOOL; (*Konc. poloha klapky K2 zavreno*)
		bK3_od : BOOL; (*Konc. poloha klapky K3 otevreno*)
		bK3_cd : BOOL; (*Konc. poloha klapky K3 zavreno*)
		bM03ptc : BOOL; (*teplota motoru M06A prekrocena (PTC)*)
		bY2 : BOOL; (*vrata kotle,  otevreno*)
		bY3 : BOOL; (*vrata kotle, zavreno*)
		bY41 : BOOL; (*posun rostu, poloha vpredu*)
		bY42 : BOOL; (*posun rostu, poloha vzadu*)
		bY5 : BOOL; (*tlacna deska, v kotli*)
		bY6 : BOOL; (*tlacna deska, venku z kotle*)
		bY7 : BOOL; (*uzaver vrat kotle, otevreno*)
		bY8 : BOOL; (*uzaver vrat kotle, zavreno*)
		bX10 : BOOL; (*pasovy dopravnik, fotosensor u kotle*)
		bX11 : BOOL; (*pasovy dopravnik, fotosensor nakladaci*)
		bX12 : BOOL; (*pasovy dopravnik, fotosensor na konci (od kotle)*)
		bTS1 : BOOL; (*vysoka teplota oleje hydrauliky*)
		bLS1 : BOOL; (*nizka hladina oleje hydrauliky*)
		bHA02 : BOOL; (*zaplaveni kotelny*)
		bHS2 : BOOL; (*havarijni tlak vody v kotly, nizky*)
		bHS1 : BOOL; (*havarijni tlak vody v kotly, vysoky*)
		bHS3 : BOOL; (*havarijni vysoka teplota vody v kotly*)
		bHA04_1 : BOOL; (*detektor CO - uroven 1*)
		bHA04_2 : BOOL; (*detektor CO - uroven 2*)
		bHA04_3 : BOOL; (*detektor CO - uroven 3*)
		bHA03 : BOOL; (*detektor koure*)
		bQ02 : BOOL; (*vykon kotle, pulsy*)
		bY2Y3auto : BOOL; (*vrata kotle automaticky*)
		bY7Y8auto : BOOL; (*uzaver vrat automaticky*)
		bY5Y6auto : BOOL; (*tlacna deska automaticky*)
		bY4auto : BOOL; (*rost automaticky*)
	END_STRUCT;
	tDOs : 	STRUCT 
		bZ04go1 : BOOL; (*retezovy dopravnik, start smer 1*)
		bZ04go2 : BOOL; (*retezovy dopravnik, start smer 2*)
		bZ08go1 : BOOL; (*senkovy dopravnik vnitrni, start*)
		bZ09go1 : BOOL; (*senkovy dopravnik vnejsi, start*)
		bZ03go : BOOL; (*hydraulicky agregat, start*)
		bZ19go : BOOL; (*vetrani kotelny - zaluzie*)
		bZ05go : BOOL; (*chladici cerpadlo kotle, start*)
		bZ10go1 : BOOL; (*senkovy dopravnik vnejsi, start*)
		bUPSoff : BOOL; (*cas. zpozdeni pro vypnuti UPS*)
		bZ21go : BOOL; (*vetrani kotelny, start*)
		bAshRemoval : BOOL; (*odpopeleni, start*)
		bZ15go : BOOL; (*cerpadlo topneho okruhu, start*)
		bZ17go : BOOL; (*ventilator vetrani kotelny, start*)
		bY1 : BOOL; (*odlehceni hydrauliky, jen pro vrata*)
		bY2 : BOOL; (*vrata otevrit*)
		bY3 : BOOL; (*vrata zavrit*)
		bY4 : BOOL; (*posun rostu*)
		bY5 : BOOL; (*tlacna deska do kotle*)
		bY6 : BOOL; (*tlacna deska od kotle*)
		bY7 : BOOL; (*uzaver vrat, odemknout*)
		bY8 : BOOL; (*uzaver vrat, zamknout*)
		bAS1 : BOOL; (*sirena (0=houka)*)
		bH11 : BOOL; (*zelena signalka*)
		bH12 : BOOL; (*cervena signalka*)
		bNoBale : BOOL; (*signalizace do nadrazeneho systemu*)
		bKA114 : BOOL;
		bKA115 : BOOL;
		bKA116 : BOOL;
		bKA117 : BOOL;
	END_STRUCT;
	tCPU : 	STRUCT 
		usNodeSwitch : USINT;
		usBatteryStatus : USINT;
		iTemperature : INT;
		iTemperatureENV : INT;
		bBusPowerSupplyWarning : ARRAY[1..4]OF BOOL;
		bIOpowerSupplyWarning : ARRAY[1..4]OF BOOL;
	END_STRUCT;
	tPLC : 	STRUCT 
		cpu : tCPU;
		module : ARRAY[1..20]OF tModuleDiag;
		sAI : tAIs;
		sAO : tAOs;
		sDI : tDIs;
		sDO : tDOs;
		PrimaryAir : tFMios;
		SecondaryAir : tFMios;
		FlueGas : tFMios;
		MainPump : tFMios;
	END_STRUCT;
	tFMouts : 	STRUCT 
		bSwitchOn : BOOL;
		bDisableVoltage : BOOL; (*0=disable voltage*)
		bEstop : BOOL; (*0=estop*)
		bEnableOperation : BOOL; (*run command*)
		bReverse : BOOL;
		bReset : BOOL; (*reset fault*)
		uiControlWord : UINT; (*Control Word*)
		iSpeedSP : INT; (*speed setpoint [rpm]*)
	END_STRUCT;
	tFMins : 	STRUCT 
		bModuleOK : BOOL; (*module ok, communication ok*)
		bReadyToSwitchOn : BOOL;
		bSwitchedOn : BOOL;
		bRunning : BOOL;
		bFault : BOOL; (*fault*)
		bAlarm : BOOL;
		iSpeed : INT; (*rpm*)
		iTorque : INT; (*%*)
		iThermalProtect : UINT;
		iCurrent : UINT; (*0.1A*)
	END_STRUCT;
	tFMios : 	STRUCT 
		in : tFMins;
		out : tFMouts;
	END_STRUCT;
	tOutputCfg : 	STRUCT 
		rOutputPerPulse : REAL; (*kW per pulse*)
	END_STRUCT;
	tOutputCmd : 	STRUCT 
		bReset : BOOL; (*reset user totalizer*)
	END_STRUCT;
	tOutput : 	STRUCT 
		bPulse : BOOL; (*pulse*)
		rActual : REAL; (*actual output*)
		rTotal : LREAL; (*totalizer*)
		rUserTotal : LREAL; (*user totalizer*)
		sCfg : tOutputCfg;
		sCmd : tOutputCmd;
	END_STRUCT;
	tComm : 	STRUCT 
		EventPV1 : BOOL; (*command to start reading*)
		LocalPV1 : ARRAY[0..6]OF INT; (*array to read the values from the slave*)
		EventPV2 : BOOL; (*command to start reading*)
		LocalPV2 : ARRAY[0..3]OF INT; (*array to read the values from the slave*)
	END_STRUCT;
	tRetain : 	STRUCT 
		s3wValveCfg : t3wValveCfg;
		sAccuTankCfg : tAccuTankCfg;
		sBoilerCfg : tBoilerCfg;
		sBoilerRoomCfg : tBoilerRoomCfg;
		sCirculationPumpCfg : tCirculationPumpCfg;
		sDoorCfg : tDoorCfg;
		sEquitermCfg : tEquitermCfg;
		sAshRemovalCfg : tAshRemovalCfg;
		sFlueGasCfg : tFlueGasCfg;
		sGrateCfg : tGrateCfg;
		sHydraulicCfg : tHydraulicCfg;
		sMainPumpCfg : tMainPumpCfg;
		sPrimaryAirCfg : tPrimaryAirCfg;
		sPusherCfg : tPusherCfg;
		sSecondaryAirCfg : tSecondaryAirCfg;
		sStrawCfg : tStrawCfg;
	END_STRUCT;
	t3wValveCfg : 	STRUCT 
		rSP : REAL; (*setpoint*)
		rKp : REAL; (*pid proporcional gain*)
		rTn : REAL; (*pid integral action time [s]*)
	END_STRUCT;
	tAccuTankCfg : 	STRUCT 
		rBypassDuration : REAL; (*lengh of the bypass duration*)
	END_STRUCT;
	tBoilerRoomCfg : 	STRUCT 
		rTempH : REAL; (*room temperature - starts ventilation*)
		rTempHH : REAL; (*triggers alarm*)
		rCOlev1 : REAL; (*CO level 1 - starts ventilation*)
		rCOlev2 : REAL; (*CO level 2 - open blinds*)
		rCOlev3 : REAL; (*CO level 3 - stop technology*)
	END_STRUCT;
	tCirculationPumpCfg : 	STRUCT 
		rDelay : REAL; (*stop delay after boiler operate stop*)
	END_STRUCT;
	tDoorCfg : 	STRUCT 
		rTimeout : REAL; (*reaching end positon timeout*)
		rTimeoutLock : REAL; (*reaching end pos timeout of the sure lock*)
	END_STRUCT;
	tEquitermCfg : 	STRUCT 
		rTadd : REAL; (*pricita se k zadane hodnote teploty v TO*)
		arTtv : ARRAY[1..14]OF REAL; (*teplota topne vody*)
		arTout : ARRAY[1..14]OF REAL; (*venkovni teplota*)
	END_STRUCT;
	tAshRemovalCfg : 	STRUCT 
		rIdle : REAL; (*idle time*)
		rMove : REAL; (*move time*)
		rIdle2 : REAL; (*idle for z08*)
		rMove2 : REAL; (*move time for z08*)
	END_STRUCT;
	tFlueGasCfg : 	STRUCT 
		arSP : ARRAY[0..6]OF REAL;
		rKp : REAL; (*pid proporcional gain*)
		rTn : REAL; (*pid integral action time [s]*)
		rOverideAddSP : REAL; (*add to SP while pusher is running*)
		rOveride : REAL; (*overide period*)
		rMaxRPM : REAL; (*nominalni otacky pohonu (ze stitku)*)
	END_STRUCT;
	tGrateCfg : 	STRUCT 
		rTimeout : REAL; (*timeout reaching the end position*)
		arCycleTime : ARRAY[0..3]OF REAL; (*grate move cycling frequency*)
		rEnableDelay : REAL; (*move delay after the straw bale stoke*)
	END_STRUCT;
	tHydraulicCfg : 	STRUCT 
		rStopDelay : REAL; (*stop delay*)
	END_STRUCT;
	tMainPumpCfg : 	STRUCT 
		rDelay : REAL; (*stop delay after boiler operate stop*)
		rMaxRPM : REAL; (*max motor speed*)
		rUpsRPM : USINT; (*motor speed when power failure*)
	END_STRUCT;
	tPusherCfg : 	STRUCT 
		rTimeout : REAL; (*reaching end positon timeout*)
		arCycleTime : ARRAY[0..3]OF REAL; (*pushing frequency*)
		rEnableDelay : REAL; (*move delay after the straw bale stoke*)
		usCycle : USINT; (*how many times to cycle to determine no fuel*)
		rCheckNoFuelDelay : REAL; (*wait after cycling to check temperatures to determine no fuel*)
		rGoInDisabled : REAL; (*delka kroku prikladani tlackou*)
		rGoInEnable : REAL; (*delka kroku cekani pri priladani tlackou*)
		usStokesPerOneBale : USINT; (*how many pusches per one bale*)
	END_STRUCT;
	tSecondaryAirCfg : 	STRUCT 
		arSP : ARRAY[0..6]OF REAL;
		rKp : REAL; (*pid proporcional gain*)
		rTn : REAL; (*pid integral action time [s]*)
		rFMmanSP : REAL; (*FM power Setpoint while pusher is running [%] *)
		rMaxRPM : REAL; (*nominalni otacky pohonu (ze stitku)*)
	END_STRUCT;
	tBoilerCfg : 	STRUCT 
		rWaterOutTempHigh : REAL; (*high limit outlet water temperature*)
		rWaterOutTempMax : REAL; (*max outlet water temperature*)
		rWaterOutTempMin : REAL; (*min outlet water temperature*)
		arCycleTime : ARRAY[0..3]OF REAL; (*cycle time set*)
		rBlockPowerChange : REAL; (*block power change timer*)
		rO2low : REAL; (*limit for low level of oxygen*)
		rStop : REAL; (*stop delay (all fuel on the grate has to burn)*)
	END_STRUCT;
	tStrawCfg : 	STRUCT 
		rConveyorMaxTransportTime : REAL; (*doba prejezdu nakladaciho dopravniku z jednoho konce na druhy*)
		rMaxConveyorToDoorTime : REAL; (*max doba pro nalozeni z nakladaciho na vrata*)
		rConveyorReleaseDoor : REAL; (*po nalozeni baliku na vrata dopravnik couvne s nakladacim dopravnikem*)
		rMaxConveyorToDoorOffDelayTime : REAL; (*po nalozeni baliku na vrata jeste kousek naklada*)
		rBale1ToOn : REAL;
	END_STRUCT;
END_TYPE
